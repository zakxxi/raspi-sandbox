import math
import time
import random
import liblo
from bootstrap import *

led.all_off()

server = liblo.Server(2121)

def off(path,args):
    led.all_off()
    print "Switch off leds"

def pixels(path,args):
    s = args
   #remapping des donnees string vers int
    irgb = [int(i) for i in s[0].split(',')]
    i = irgb[0]
    r = irgb[1]
    g = irgb[2]    
    b = irgb[3]       
    led.set(i,Color(r,g,b,1))

    led.update()
    print "Set %d with r: %d, g: %d, b: %d" % (i,r,g,b)
    
def update(path,args):
    led.update()
    print "Update leds"
    
server.add_method("/pixels", 'ssssssssssssssssssssssssssssssssssssssssssssssss', pixels)
server.add_method("/off", '', off)

print "\n ***** Starting liliServer *****"
while True:
    server.recv(100)
    