import spi
import time
import liblo

spi.openSPI(speed=1000000)

server = liblo.Server(8000)

def pixel(path, args):
    p, r, g, b = args
    spi.transfer((p, r, g, b))
#print "setting %d to (%d,%d,%d)" % (p, r, g, b)

 
server.add_method("/pixel", 'iiii', pixel)

print "\n ***** Starting testServer *****"
while True:
    server.recv(100)