#!/usr/bin/env python

import math
import time
import random
import liblo
from bootstrap import *

led.all_off()

server = liblo.Server(8000)

def fill(path, args):
    r,g,b = args
    led.fill(Color(r,g,b,1))
    led.update()
    print "Fill with r: %d, g: %d, b: %d " % (r,g,b)
    
def off(path,args):
    led.all_off()
    print "Switch off leds"

def pixel(path,args):
    i,r,g,b = args
    led.set(i,Color(r,g,b,1))
    print "Set %d with r: %d, g: %d, b: %d" % (i,r,g,b)
    
def update(path,args):
    led.update()
    print "Update leds"
    
server.add_method("/fill", 'iii', fill)
server.add_method("/pixel", 'iiii', pixel)
server.add_method("/off", '', off)
server.add_method("/update", '', update)

print "\n ***** Starting liliServer *****"
while True:
    server.recv(100)
    