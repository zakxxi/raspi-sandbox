import math
import time
import random
import liblo
from bootstrap import *

led.all_off()

server = liblo.Server(8000)

def fill(path, args):
    s = args
    #remapping des donnees string vers int
    rgb = [int(i) for i in s[0].split(',')]
    r = rgb[0]
    g = rgb[1]
    b = rgb[2]    
    led.fill(Color(r,g,b,1))
    led.update()
    #print "Fill with r: %d, g: %d, b: %d " % (r,g,b)
    
def off(path,args):
    s = args
    led.all_off()
    print "Switch off leds"

def pixel(path,args):
    s = args
   #remapping des donnees string vers int
    irgb = [int(i) for i in s[0].split(',')]
    i = irgb[0]
    r = irgb[1]
    g = irgb[2]    
    b = irgb[3]       
    led.set(i,Color(r,g,b,1))
    led.update()
    print "Set %d with r: %d, g: %d, b: %d" % (i,r,g,b)
    
def update(path,args):
    led.update()
    print "Update leds"
    
server.add_method("/fill", 's', fill)
server.add_method("/pixel", 's', pixel)
server.add_method("/off", 's', off)
server.add_method("/update", 's', update)

print "\n ***** Starting liliServer *****"
while True:
    server.recv(100)
    