import math
import time
import random
#import liblo
from bootstrap import *
from random import randint
from array import array

led.all_off()

start =  []
sign = []
color = []
nb = 20

for p in range(nb):
    start.append(randint(0,192))
    if(randint(0,100) < 50):
        sign.append(-randint(1,3))
    else :
        sign.append(+randint(1,3))
    color.append(ColorHSV(randint(0,360),1,1.0))

while True:
    for p in range(nb):
         led.set(start[p],Color(0,0,0))
         start[p] = (start[p]+sign[p])
         if ( start[p] <0 ):
            start[p]=191
         if ( 191<start[p] ):
            start[p]=0
         led.set(start[p],color[p].get_color_rgb())
    led.update()
    i = randint(0,nb-1)
    if(randint(0,100)  < 10):
        sign[i] = -1*sign[i] 

